# Prerequisites

 * Python3
 * termcolor module
 

# Run

Scan the seventh entry from given .fasta file

```
  ./alignotate.py data/tcoffee.fasta -s 6
```

Check all the entries

```
  ./alignotate.py data/tcoffee.fasta
```

Do not filter annotations

```
  ./alignotate.py data/tcoffee.fasta -v
```
