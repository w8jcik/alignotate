#!/usr/bin/python3

### Parse CLI options and parameters

from optparse import OptionParser

parser = OptionParser("usage: %prog [options] sequences.fasta")

parser.add_option("-s", "--selected-accession", metavar="NUMBER", dest="selected_accession", default=-1, type="int")
parser.add_option("-v", action="store_true", dest="verbose", help="present all annotations")

(options, args) = parser.parse_args()

if len(args) != 1:
  exit('Wrong number of arguments.')


### Read the sequences from given fasta file

from Bio import SeqIO

handle = open(args[0], "rU")
seqs_with_long_id = SeqIO.to_dict(SeqIO.parse(handle, "fasta"))
handle.close()

seqs = {}
for key, seq in seqs_with_long_id.items():
  seqs[key.split('/')[0]] = str(seq.seq)
  
seqs_with_long_id = None

from Bio import ExPASy
from Bio import SwissProt

accessions = list(seqs.keys())  # ["ENO11_SCHPO", "ENO12_SCHPO", ...]
accessions.sort()

from termcolor import colored

print("Sequences present in the file:")

if options.selected_accession > -1:
  i = 0;
  for accession in accessions:
    
    if options.selected_accession == i:
      entry = colored(accession, 'green')
      
    else:
      entry = accession
      
    print("%2d, %s" % (i, entry), end=" ")
    i += 1
  accessions = accessions[options.selected_accession:options.selected_accession+1]
  
  print("\n")


### Get the annotations from SwissProt database

chains = {}

print("Retrieving SwissProt data...")

for accession in accessions:
  print("%s..." % colored(accession, 'green'))
  
  handle = ExPASy.get_sprot_raw(accession)
  record = SwissProt.read(handle)

  max_length = 9999999
  exclusion_list = ['CHAIN']  # 'CHAIN'-type annotations cover whole chain and are useless for verification of MSA

  if not options.verbose:
    exclusion_list = ['CHAIN', 'MOD_RES', 'CONFLICT', 'MUTAGEN', 'COMPBIAS', 'STRAND', 'HELIX', 'TURN']
    max_length = 10
    
  chains[accession] = {
    'sequence': record.sequence,
    'features': filter(lambda feature: feature[0] not in exclusion_list and feature[2] - feature[1] < max_length, record.features)
  }

print("\n")


### Map positions of annotations from unaligned sequences to aligned ones
### Warn about annotated but unaligned regions 

import re

take_n_neighbours = 3

for accession, chain in chains.items():
  differences_count = 0
  
  print("Analysing annotations of sequence %s\n" % colored(accession, 'green'))
  
  in_file = seqs[accession].replace('-', '')
  in_db = chain['sequence']
  
  if in_db != in_file:
    print(colored("Sequence in the database differs from the one found in MSA. The annotations might be incorrect\n", 'red'))
    print('file: %s\n  db: %s' % (in_file, in_db))
  
  for feature in chain['features']:
    begin = feature[1]
    end = feature[2]
    length = end - begin
    
    preffered_extended_begin = begin - take_n_neighbours - 1
    preffered_extended_end = end + take_n_neighbours + 1
    
    total_length = len(chain['sequence'])
    
    extended_begin = max(preffered_extended_begin, 0)
    extended_end = min(preffered_extended_end, total_length)

    print("%14s\t%s%s%s" % (
      feature[0].capitalize().replace('_', ' '),
      chain['sequence'][extended_begin:begin-1],
      colored(chain['sequence'][begin-1:end], 'green'),
      chain['sequence'][end:extended_end-1]
    ), end="\t\t")

    # A pattern ignoring dashes

    intermediate_pattern = chain['sequence'][extended_begin:extended_end-1]
    pattern = "-*".join(list(intermediate_pattern))
    
    if len(re.findall(pattern, seqs[accession])) > 1:
      # This shouldn't happen, but check to be sure.
      exit('ambigious match')
    
    result = re.search(pattern, seqs[accession])
    
    if result:
      location = result.start() - (extended_begin - begin)
      content = seqs[accession][location-1:location+length]
      
      # Now when we have a position of annotation in an aligned sequence we can
      # try to match it in the others from our MSA.
      
      print(location, "\t", colored(content, 'green'), end=" ")
      
      for key, seq in seqs.items():
        found = seq[location-1:location+length]
        
        if found != content:
          print("%s:%s" % (key, colored(found, 'red')), end=", ")
          differences_count += 1
    else:
      print(colored("'%s' pattern not found" % intermediate_pattern, 'red'), end=" ")
      print("position in initial sequence %d:%d" % (feature[1], feature[2]), end='')
      differences_count += 1
      
    print()
    
  print()
  if differences_count == 0:
    print(colored("All annotated residues from %s are at the same positions within whole MSA." % accession, 'green'))
  else:
    print(colored("Positions of annoted residues vary within MSA, %d times in total." % differences_count, 'red'))
    
  print()
